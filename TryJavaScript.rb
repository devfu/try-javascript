#! /usr/bin/env ruby
%w( rubygems sinatra haml sass ).each {|lib| require lib }

get '/' do
  haml :index
end

get '/tutorials/:tutorial.html' do
  template = File.join options.views, 'tutorials', "#{ params[:tutorial] }.haml"
  if File.file? template
    haml File.read(template), :layout => :tutorial_layout
  else
    "Tutorial not found: #{ params[:tutorial] }"
  end
end

get '/irb.cgi' do
  "called irb.cgi with command #{ params[:cmd].inspect }"
end
