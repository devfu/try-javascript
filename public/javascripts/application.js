// used to build up commands
window.selenium_command = '';

// helper to puts something out to the terminal
window.print = function(str) {
  MouseApp.Manager.activeTerm.puts(str);
}
window.puts = function(str) {
  window.print(str + "\n");
}

$(document).ready(function(){

  // when a special field is changed, take the text and 
  // enter it into the "IRB" console ... this is for Selenium tests
  // because Selenium's keyPress can't actually emulate all keys  :(
  $('#selenium').change(function(){
    var command = $(this).val();
    $(this).val('');

    window.selenium_command = window.selenium_command + command.replace(/\|$/, '');

    if (/\|$/.test(command) == false) {
      MouseApp.Manager.activeTerm.puts(window.selenium_command);
      MouseApp.Manager.activeTerm.onKeyEnter();
      window.selenium_command = '';
    }
  });

});
