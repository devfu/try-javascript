// JavaScript "IRB" ... basically just gets the result of a command passed
//
// If anything we might wrap all calls in an object so we've always eval-ing 
// in a particular scope.  But, for now, we just eval and inspect the result.
var JavaScriptIRB = {

  variables_added_to_window: [],

  getResult: function(cmd) {
    cmd = $.trim(cmd);

    if (cmd == 'reset') {
      var vars_reset = JavaScriptIRB.variables_added_to_window.join(', ');
      $.each(JavaScriptIRB.variables_added_to_window, function(i, var_name){
        window[var_name] = null;
      });
      JavaScriptIRB.variables_added_to_window = [];

      return '=> \033[1;20m' + 'reset variables ' + vars_reset;
    }
    
    // Handle "var X ="
    var var_match = cmd.match(/^var (\w+) =/);
    if (var_match != null) {
      var var_name = var_match[1];
      if ($.inArray(var_name, JavaScriptIRB.variables_added_to_window) == -1)
        JavaScriptIRB.variables_added_to_window.push(var_name);
      cmd = cmd.replace(/^var (\w+) =/, 'window.' + var_name + ' =');
    }

    // Handle "x ="
    var implicit_var_match = cmd.match(/^(\w+) =/);
    if (implicit_var_match != null) {
      var var_name = implicit_var_match[1];
      if ($.inArray(var_name, JavaScriptIRB.variables_added_to_window) == -1)
        JavaScriptIRB.variables_added_to_window.push(var_name);
    }

    try {
      console.log('command: ' + cmd);
      console.log('[command is inspected here] eval(' + JavaScriptIRB.inspect(cmd) + ')');
      var result = eval(cmd);
    } catch (err) {
      var result = err.toString();
    }
    return '=> \033[1;20m' + JavaScriptIRB.inspect(result);
  },

  // now using toJSON jquery plugin
  inspect: function(obj) {
    if (obj === null)
      return 'null';

    if (typeof(obj) == 'undefined') {
      console.log('typeof was undefined');
      return 'undefined';
    }

    if (typeof(obj) == 'string')
      return '"' + obj + '"';

    // toJSON doesn't seem to properly return arrays with 
    // 0 values or 1 value
    if (/^function Array/.test(obj.constructor.toString())) {
      if (obj.length == 0) {
        return "[]";
      } else if (obj.length == 1) {
        return '[' + obj[0] + ']';
      }
    }

    return $.toJSON(obj);
  }

}
